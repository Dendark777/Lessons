﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using UnityEditor;
using UnityEngine;

namespace Assets.ToolsEditor.ButtonEditor
{
    [CustomEditor(typeof(object), true, isFallback = false)]
    [CanEditMultipleObjects]
    public class ButtonEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            foreach (var target in targets)
            {
                var mis = target.GetType().GetMethods().Where(m => m.GetCustomAttributes().Any(a => a.GetType() == typeof(ButtonEditorAttribute)));
                if (mis == null)
                {
                    continue;
                }
                foreach (var mi in mis)
                {
                    if (mi == null)
                    {
                        continue;
                    }
                    var atribute = (ButtonEditorAttribute)mi.GetCustomAttribute(typeof(ButtonEditorAttribute));
                    if (GUILayout.Button(atribute.Text))
                    {
                        mi.Invoke(target, null);
                    }
                }
            }
        }
    }
}
