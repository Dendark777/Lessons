using System;

namespace Assets.ToolsEditor.ButtonEditor
{
    [AttributeUsage(AttributeTargets.Method)]
    public class ButtonEditorAttribute : Attribute
    {
        public string Text { get; set; }

        public ButtonEditorAttribute(string text)
        {
            Text = text;
        }
    }
}

