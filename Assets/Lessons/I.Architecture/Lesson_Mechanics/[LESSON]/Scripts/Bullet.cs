﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Lessons.Architecture.Mechanics
{
    public class Bullet : MonoBehaviour
    {
        [SerializeField]
        private EventReceiver _startFlight;

        public void Init()
        {
            _startFlight.Call();
        }
    }
}
