﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Unity.VisualScripting;
using UnityEngine;

namespace Lessons.Architecture.Mechanics
{
    public class ShotMechanics : MonoBehaviour
    {
        [SerializeField]
        private EventReceiver attackReceiver;

        [SerializeField]
        private TimerBehaviour _countdown;

        [SerializeField]
        private IntBehaviour damage;

        [SerializeField]
        private Transform _unit;

        [SerializeField]
        private Bullet _bullet;

        private void OnEnable()
        {
            this.attackReceiver.OnEvent += this.OnRequiestShot;
        }

        private void OnDisable()
        {
            this.attackReceiver.OnEvent -= this.OnRequiestShot;
        }

        private void OnRequiestShot()
        {
            if (_countdown.IsPlaying)
            {
                return;
            }
            var bullet = Instantiate(_bullet, transform.position, new Quaternion());

            bullet.Init();
            //Сбросить и запустить таймер снова:
            _countdown.ResetTime();
            _countdown.Play();
        }
    }
}
