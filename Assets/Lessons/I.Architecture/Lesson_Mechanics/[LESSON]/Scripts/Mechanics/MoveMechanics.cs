﻿using Assets.ToolsEditor.ButtonEditor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Lessons.Architecture.Mechanics
{
    public class MoveMechanics : MonoBehaviour
    {
        [SerializeField]
        private EventReceiver _moveLeftReceiver;
        [SerializeField]
        private EventReceiver _moveRightReceiver;
        [SerializeField]
        private EventReceiver _moveUpReceiver;
        [SerializeField]
        private EventReceiver _moveDownReceiver;

        [SerializeField]
        private Transform _unit;

        [SerializeField]
        private FloatBehaviour _speed;

        private void OnEnable()
        {
            _moveLeftReceiver.OnEvent += OnRequiestMoveLeft;
            _moveRightReceiver.OnEvent += OnRequiestMoveRight;
            _moveUpReceiver.OnEvent += OnRequiestMoveUp;
            _moveDownReceiver.OnEvent += OnRequiestMoveDown;
        }

        private void OnDisable()
        {
            _moveLeftReceiver.OnEvent -= OnRequiestMoveLeft;
            _moveRightReceiver.OnEvent -= OnRequiestMoveRight;
            _moveUpReceiver.OnEvent -= OnRequiestMoveUp;
            _moveDownReceiver.OnEvent -= OnRequiestMoveDown;
        }
        private void OnRequiestMoveLeft()
        {
            _unit.position += Vector3.left * _speed.Value;
        }

        private void OnRequiestMoveRight()
        {
            _unit.position += Vector3.right * _speed.Value;
        }

        private void OnRequiestMoveUp()
        {
            _unit.position += Vector3.forward * _speed.Value;
        }

        private void OnRequiestMoveDown()
        {
            _unit.position += Vector3.back * _speed.Value;
        }
    }
}
