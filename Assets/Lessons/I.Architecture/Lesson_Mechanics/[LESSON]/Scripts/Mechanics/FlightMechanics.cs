﻿using Lessons.Architecture.Mechanics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Lessons.I.Architecture.Lesson_Mechanics._LESSON_.Scripts.Mechanics
{
    public class FlightMechanics : MonoBehaviour
    {
        [SerializeField]
        private EventReceiver _start;

        [SerializeField]
        private Transform _unit;

        [SerializeField]
        private PeriodBehaviour _flight;

        [SerializeField]
        private TimerBehaviour _flightTime;

        [SerializeField]
        private FloatBehaviour _speed;

        private void OnEnable()
        {
            _start.OnEvent += OnStart;
            _flight.OnEvent += OnFlight;
            _flightTime.OnEnded += OnEnded;
        }

        private void OnDisable()
        {
            _start.OnEvent -= OnStart;
            _flight.OnEvent -= OnFlight;
            _flightTime.OnEnded -= OnEnded;
        }
        private void OnStart()
        {
            _flightTime.Play();
            _flight.Play();
        }
        private void OnEnded()
        {
            _flight.Stop();
            _flightTime.Stop();
            Destroy(_unit.gameObject);
        }

        private void OnFlight()
        {
            _unit.position += Vector3.right * _speed.Value / 20;
        }
    }
}
