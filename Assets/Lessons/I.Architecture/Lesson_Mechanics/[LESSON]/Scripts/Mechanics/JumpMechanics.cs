﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Lessons.Architecture.Mechanics
{
    public class JumpMechanics : MonoBehaviour
    {
        [SerializeField]
        private EventReceiver _jumpReceiver;

        [SerializeField]
        private Transform _unit;

        [SerializeField]
        private FloatBehaviour _power;

        [SerializeField]
        private PeriodBehaviour _upPeriod;
        [SerializeField]
        private PeriodBehaviour _downPeriod;

        private void OnEnable()
        {
            _jumpReceiver.OnEvent += OnRequiestJump;
            _upPeriod.OnEvent += OnUpJump;
            _downPeriod.OnEvent += OnDownJump;
        }

        private void OnDisable()
        {
            _jumpReceiver.OnEvent -= OnRequiestJump;
            _upPeriod.OnEvent -= OnUpJump;
            _downPeriod.OnEvent -= OnDownJump;
        }
        private void OnRequiestJump()
        {
            if (_unit.position.y != 0)
            {
                return;
            }
            _upPeriod.Play();

        }
        private void OnUpJump()
        {
            _unit.position += Vector3.up * _power.Value / 20;
            if (_unit.position.y + _power.Value / 20 >= _power.Value)
            {
                _unit.position = new Vector3(_unit.position.x, _power.Value, _unit.position.z);
                _upPeriod.Stop();
                _downPeriod.Play();
            }
        }

        private void OnDownJump()
        {
            _unit.position += Vector3.down * _power.Value / 20;
            if (_unit.position.y - _power.Value / 20 <= 0)
            {
                _unit.position = new Vector3(_unit.position.x, 0, _unit.position.z);
                _downPeriod.Stop();
            }
        }
    }
}
