using Assets.ToolsEditor.ButtonEditor;
using System;
using UnityEngine;

namespace Lessons.Architecture.Mechanics
{
    public sealed class EventReceiver : MonoBehaviour
    {
        public event Action OnEvent;

        [ButtonEditor("Выполнить")]
        public void Call()
        {
            Debug.Log($"Event {name} was received!");
            this.OnEvent?.Invoke();
        }
    }
}